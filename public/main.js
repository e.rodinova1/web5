function updatePrice() {

  let select = document.getElementsByName("prodType")[0];
  let price = 0;
  let prices = getPrices(); // Array 
  let priceIndex = parseInt(select.value) - 1;
  
  if (priceIndex >= 0) {
    price = prices.prodTypes[priceIndex];
  }
  

  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = (select.value == "3" ? "block" : "none");

  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function(radio) {
    if (radio.checked) {
      let optionPrice = prices.prodOptions[radio.value];
      if (optionPrice !== undefined) {
        price += optionPrice;
      }
    }
  });

  let checkDiv = document.getElementById("checkboxes");
  checkDiv.style.display = (select.value == "3" ? "none" : "block");

  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    if (checkbox.checked) {
      let propPrice = prices.prodProperties[checkbox.name];
      if (propPrice !== undefined) {
        price += propPrice;
      }
    }
  });
  
  let prodPrice = document.getElementById("prodPrice");
  prodPrice.innerHTML = price + " ÑÑÐ±Ð»ÐµÐ¹";
}

function getPrices() {
  return {
    prodTypes: [100, 500, 990],
    prodOptions: {
      option2: 12,
      option3: 6,
    },
    prodProperties: {
      prop1: 3,
      prop2: 2,
    }
  };
}

window.addEventListener('DOMContentLoaded', function (event) {

  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = "none";

  let select = document.getElementsByName("prodType")[0];

  select.addEventListener("change", function(event) {
    let target = event.target;
    console.log(target.value);
    updatePrice();
  });
  
 
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function(radio) {
    radio.addEventListener("change", function(event) {
      let r = event.target;
      console.log(r.value);
      updatePrice();
    });
  });

  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    checkbox.addEventListener("change", function(event) {
      let c = event.target;
      console.log(c.name);
      console.log(c.value);
      updatePrice();
    });
  });

  updatePrice();
});

function onClick() {
	alert("ÐÐ¾Ð·ÑÐ°Ð²Ð»ÑÐµÐ¼! ÐÑ ÑÐ´Ð°ÑÐ½Ð¾ Ð¾ÑÐ¾ÑÐ¼Ð¸Ð»Ð¸ Ð·Ð°ÐºÐ°Ð·!");
}
